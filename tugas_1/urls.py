"""tugas_1 URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from django.conf import settings
from django.conf.urls.static import static

"""
Isi url halaman di bagian folder tugas1/urls.py yg disini dia bakal include ke urls.py yg di app
!!~ Jadi ga perlu rubah2 urls.py yg disini lagi ~!!
"""
urlpatterns = [
    path('admin/', admin.site.urls),
    path('', include('tugas1.urls')),
    path('user/', include('login.urls')),
    path('testimoni/', include('testimoni.urls')),
]

if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_URL)
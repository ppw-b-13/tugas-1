from django.urls import path, include
from . import views

urlpatterns = [
    path('', views.testimoni_page),
    path('addtesti/', views.add_testimoni),
]

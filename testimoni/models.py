from django.db import models

# Create your models here.
from django.utils import timezone


class testimoniUser(models.Model):
    comment = models.CharField(max_length=300)
    time = models.DateTimeField(default=timezone.now)

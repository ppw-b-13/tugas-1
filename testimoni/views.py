import datetime
from django.shortcuts import render
from testimoni.forms import testimoni
from testimoni.models import testimoniUser


def testimoni_page(request):
    form = testimoni()
    date = datetime.datetime.now()
    response = {'form': form, 'get_comment': testimoniUser.objects.all().order_by('-time'), 'datetime': date}
    return render(request, 'testimoni.html', response)


response = {}
def add_testimoni(request):
    if request.method == "POST":
        print("masuk nih mau post")
        form = testimoni(request.POST or None)
        if form.is_valid():
            response['comment'] = request.POST['comment']
            testi = testimoniUser(comment=response['comment'])
            testi.save()
            return render(request, 'testimoni.html', response)
        else:
            print("not valid")
    else:
        print("ke else terakhir")

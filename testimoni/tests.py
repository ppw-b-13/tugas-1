from django.test import TestCase, Client
from django.urls import resolve

from . import views
# Create your tests here.

class testing_response_page(TestCase):

    def test_page_testimoni(self):
        response = Client().get('/testimoni/')
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'testimoni.html')

class testing_function_in_views(TestCase):

    def test_testi_func(self):
        found = resolve('/testimoni/')
        self.assertEqual(found.func, views.testimoni_page)

    def test_post_func(self):
        found = resolve('/testimoni/addtesti/')
        self.assertEqual(found.func, views.add_testimoni)

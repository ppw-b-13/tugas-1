# Tugas 1 Kelompok 13 kelas PPW-B


# Members of this group:
1. Bramantio Krisno Aji
2. Stefanus Khrisna Aji Hardiyanto
3. Randy Hidayah Putra Desnantara

# Status Pipelines :
[![pipeline status](https://gitlab.com/ppw-b-13/tugas-1/badges/master/pipeline.svg)](https://gitlab.com/ppw-b-13/tugas-1/commits/master)

# Status code coverage :
[![coverage report](https://gitlab.com/ppw-b-13/tugas-1/badges/master/coverage.svg)](https://gitlab.com/ppw-b-13/tugas-1/commits/master)

# Link Herokuapp :
https://ppw-b-13.herokuapp.com/ 
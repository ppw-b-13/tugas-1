from django.contrib.auth import authenticate, logout, login
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render
# Create your views here.
from login.forms import LoginForm


def ngetes(request):
    return render(request, 'ngetes.html')

def login_view(request):
    if request.user.is_authenticated:
        return HttpResponseRedirect('/user/')
    if request.method == 'POST':
        form2 = LoginForm(request.POST or None)
        if form2.is_valid():
            username = form2.cleaned_data['username']
            password = form2.cleaned_data['password']
            donatur = authenticate(username=username, password=password)
            if donatur is not None:
                login(request, donatur)
                return HttpResponseRedirect('/')
            else:
                return HttpResponseRedirect('/user/login')
        else:
            context = {'form_login': form2}
            return render(request, 'login.html', context)
    else:
        form2 = LoginForm(request.POST or None)
        context = {'form_login': form2}
        return render(request, 'login.html', context)

def logout_view(request):
    logout(request)
    request.session.flush()
    return render(request, 'logout.html')

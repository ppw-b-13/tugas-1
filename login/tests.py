from django.test import TestCase, Client
from django.urls import resolve
from login import views
from django.test import TestCase



class testing_response_page(TestCase):

    def test_page_ngetes(self):
        response = Client().get('/user/')
        self.assertEqual(response.status_code, 200)

    def test_page_login(self):
        response = Client().get('/user/login/')
        self.assertEqual(response.status_code, 200)

    def test_page_logout(self):
        response = Client().get('/user/logout/')
        self.assertEqual(response.status_code, 200)

class testing_function_in_views(TestCase):

    def test_login(self):
        found = resolve('/user/login/')
        self.assertEqual(found.func, views.login_view)

    def test_logout(self):
        found = resolve('/user/logout/')
        self.assertEqual(found.func, views.logout_view)

    def test_ngetes(self):
        found = resolve('/user/')
        self.assertEqual(found.func, views.ngetes)

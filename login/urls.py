from django.urls import path, include
from . import views

urlpatterns = [
    path('auth/', include('social_django.urls', namespace='social')),
    path('login/', views.login_view, name='login'),
    path('logout/', views.logout_view, name='login'),
    path('', views.ngetes),
]
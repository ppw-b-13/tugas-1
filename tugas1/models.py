from django.db import models

# Create your models here.
class Registrasi(models.Model):
    Nama = models.CharField(max_length=40)
    Tanggal = models.DateField()
    Username = models.CharField(max_length=40)
    Email = models.EmailField()

class Donasi(models.Model):
    Nama = models.CharField(max_length=40)
    Email = models.EmailField()
    # Campaign = models.
    Jumlah_uang = models.DecimalField(decimal_places=1, max_digits=40, default=0)

class Berita(models.Model):
    judul = models.TextField(default="Judul")
    konten = models.TextField(default="Isi Berita")
    # imgsrc = models.CharField(max_length = 2000, default = "Ambil Gambar 'src' dari Google ya, karena kalau pakai ImageField "
    #                                     "kata django harus install pillow.")
    image = models.FileField(null=True, blank=True)
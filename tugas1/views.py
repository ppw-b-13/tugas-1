from django.contrib.auth.models import User
from django.shortcuts import render
from .forms import *
from .models import *


# Create your views here.
def homepage(request):
    return render(request, 'homepage.html')


def donasi(request):
    return render(request, 'donasi.html')


def berita(request):
    data_berita = Berita.objects.all()
    response = {'data_berita': data_berita}
    return render(request, 'berita.html', response)


def registrasi(request):
    form = FormsRegistration()
    response = {'form': form}
    return render(request, 'registrasi.html', response)


response = {}


def masuk_data(request):
    if request.method == 'POST':
        form = FormsRegistration(request.POST or None   )
        if form.is_valid():
            response['nama'] = request.POST['Nama']
            response['tanggal'] = request.POST['Tanggal']
            response['username'] = request.POST['Username']
            response['email'] = request.POST['Email']
            response['password'] = request.POST['Password']

            data_akun = Registrasi.objects.all()

            for i in data_akun:
                if response['email'] == i.Email:
                    return render(request, 'email_sama.html')

            #
            regis_save = Registrasi(Nama=response['nama'], Tanggal=response['tanggal'], Username=response['username'],
                                    Email=response['email']
                                    )
            regis_save.save()
            #
            donatur = User.objects.create_user(first_name=response['nama'], username=response['username'],
                                               email=response['email'], password=response['password']
                                               )
            donatur.save()
            #
            return render(request, 'registrasi_sukses.html', response)

    else:
        return render(response, 'donasi.html')


def konfirmasi(request):
    form = FormsDonasi()

    response = {'form_donasi': form}
    return render(request, 'konfirmasi.html', response)


response_donasi = {}


def masuk_data_donasi(request):
    if request.method == 'POST':
        form = FormsDonasi(request.POST or None, request.FILES or None)
        if form.is_valid():
            response_donasi['nama'] = request.POST['Nama']
            response_donasi['email'] = request.POST['Email']
            response_donasi['jumlah_uang'] = request.POST['Jumlah_uang']

            data_akun = Registrasi.objects.all()

            for i in data_akun:
                if response_donasi['email'] == i.Email:
                    #
                    donasi_save = Donasi(Nama=response_donasi['nama'], Email=response_donasi['email'],
                                         Jumlah_uang=response_donasi['jumlah_uang'])
                    donasi_save.save()
                    #

                    return render(request, 'konfirmasi_sukses.html', response_donasi)

            return render(request, 'email_belum_terdaftar.html')



    else:
        return render(response_donasi, 'konfirmasi.html')

from django import forms

class FormsRegistration(forms.Form):
    error_messages = {
        'required': 'Tolong isikan kolom ini'
    }
    attrs = {
        'class': 'form-control'
    }
    Nama = forms.CharField(widget=forms.TextInput(attrs=attrs), label='Nama', max_length=40, empty_value='Anonymous', required=True)
    Tanggal = forms.DateField(widget=forms.TextInput(attrs=
                                {
                                    'class':'form-control',
                                    'type':'date'
                                }), required=True)
    Username = forms.CharField(widget=forms.TextInput(attrs=attrs), label='Username', max_length=40, empty_value='username123', required=True)
    Email = forms.EmailField(widget=forms.EmailInput(attrs=attrs), required=True)
    Password = forms.CharField(widget=forms.PasswordInput(attrs = attrs), max_length=40, required=True)


class FormsDonasi(forms.Form):
    error_messages = {
        'required': 'Tolong isikan kolom ini'
    }
    attrs = {
        'class': 'form-control'
    }
    Nama = forms.CharField(widget=forms.TextInput(attrs=attrs), label='Nama', max_length=40, empty_value='Anonymous',
                           required=True)
    Email = forms.EmailField(widget=forms.EmailInput(attrs=attrs), required=True)
    # campaign dropdown
    Jumlah_uang = forms.DecimalField(widget=forms.NumberInput(attrs=attrs), required=True)
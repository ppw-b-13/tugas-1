from django.test import TestCase
from django.test import Client
from django.urls import resolve

from tugas1.models import Berita
from . import views

# Create your tests here.
class testing_homepage(TestCase):
    def test_homepage(self):
        response = Client().get('')
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'homepage.html')

    def test_homepage_func(self):
        found = resolve('/')
        self.assertEqual(found.func, views.homepage)

class testing_donasi(TestCase):
    def test_response_page(self):
        response = Client().get('/donasi/')
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'donasi.html')

    def test_donasi_func(self):
        found = resolve('/donasi/')
        self.assertEqual(found.func, views.donasi)

class testing_berita(TestCase):
    def test_response_page(self):
        response = Client().get('/berita/')
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'berita.html')

    def test_berita_func(self):
        found = resolve('/berita/')
        self.assertEqual(found.func, views.berita)

class testing_registrasi(TestCase):
    def test_registrasi_page(self):
        response = Client().get('/registrasi/')
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'registrasi.html')

    def test_registrasi_func(self):
        found = resolve('/registrasi/')
        self.assertEqual(found.func, views.registrasi)

class testing_konfirmasi(TestCase):
    def test_konfirmasi_page(self):
        response = Client().get('/konfirmasi/')
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'konfirmasi.html')

    def test_konfirmasi_func(self):
        found = resolve('/konfirmasi/')
        self.assertEqual(found.func, views.konfirmasi)


class testing_berita(TestCase):
    def test_berita_func(self):
        found = resolve('/berita/')
        self.assertEqual(found.func, views.berita)

    def test_model_can_create_new_activity(self):
        new_activity = Berita.objects.create()
        all_activities = Berita.objects.all().count()
        self.assertEqual(all_activities, 1)

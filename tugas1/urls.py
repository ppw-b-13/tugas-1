from django.urls import path
from tugas1 import views
from django.conf import settings
from django.conf.urls.static import static

"""
tambah url disini
"""
urlpatterns = [
    path('', views.homepage),
    path('donasi/', views.donasi),
    path('berita/', views.berita),
    path('registrasi/', views.registrasi),
    path('konfirmasi/', views.konfirmasi),
    path('registrasi-sukses/', views.masuk_data),
    path('konfirmasi-sukses/', views.masuk_data_donasi)
]

if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_URL)